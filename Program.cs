﻿using System;
using System.IO;

namespace Perceptron
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] inputValues = { { 0, 0 }, 
                                   { 1, 0 }, 
                                   { 0, 1 }, 
                                   { 1, 1 } };

            int[] outputValues = { 0, 1, 1, 1 };
            
            double[] weights = { GetRAndomDouble(-1, 1), GetRAndomDouble(-1, 1) };
            double bias = GetRAndomDouble(-1, 1);

            double error = 1;

            while (error > 0.5)
            {
                error = 0;
                for (int i = 0; i < 4; i++) 
                {
                    int output = CalculateOutput(inputValues[i, 0], inputValues[i, 1], bias, weights);
                    int localError = outputValues[i] - output;

                    weights[0] = inputValues[i, 0] * localError + weights[0];
                    weights[1] = inputValues[i, 1] * localError + weights[1];

                    bias += localError;

                    error += Math.Abs(localError);

                    Console.WriteLine($"W1 {weights[0]}, W2 {weights[1]}, B{bias}");
                    Console.WriteLine($"Error: {error}");
                }
            }
            

            Console.ReadKey();
        }

        private static int CalculateOutput(double input1, double input2, double bias, double[] weights) 
        {
            double result = input1 * weights[0] + input2 * weights[1] + bias;
            Console.WriteLine($"Result = {input1 * weights[0]} + {input2 * weights[1]} + {bias}.");

            /*if (result >= 0)
                return 1;
            else
                return 0;*/

            return (result >= 0) ? 1 : 0;
        }

        private static double GetRAndomDouble(double min, double max) 
        {
            Random rnd = new Random();
            return rnd.NextDouble() * (max - min) + min;
        }

        private static void SaveData(double[] weighs, double bias) 
        {
            string pathToFile = "C:";
            string fileName = "data.txt";
            string fullPath = Path.Combine(pathToFile, fileName);

            string data = $"{weighs[0]}:{weighs[1]}:{bias}";

            File.WriteAllText(fullPath, data);
        }
    }
}
